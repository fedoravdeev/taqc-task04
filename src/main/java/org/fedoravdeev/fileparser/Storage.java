package org.fedoravdeev.fileparser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Storage {

    private String filePath = new String();
    private String text = new String("Lorem ipsum dolor sit amet, nec te libris dictas. Inermis apeirian cu duo, " +
            "mel cu modo suavitate persecuti, at quando iriure interesset sea. Tota posse voluptua vim no. " +
            "Usu utinam legendos iudicabit et, melius option sit an.");

    private int flag;

    private String stringSearch;
    private String stringReplace;

    public void setParam(String[] args) {

        if (args.length < 1){
            return;
        }
        setFilePath(args[0]);
        setStringSearch(args[1]);
        flag = args.length;

        if(args.length == 3){
            setStringReplace(args[2]);
        }
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public void setStringSearch(String stringSearch) {
        this.stringSearch = stringSearch;
    }

    public void setStringReplace(String stringReplace) {
        this.stringReplace = stringReplace;
    }

    public String getStringSearch() {
        return stringSearch;
    }

    public String getStringReplace() {
        return stringReplace;
    }

    public void run() {
        if(flag == 2) {
            int count = 0;
            Pattern p = Pattern.compile(getStringSearch(), Pattern.UNICODE_CASE|Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(text);
            while(m.find()){
                count++;
            }
            System.out.println("'"+getStringSearch()+"' in text " + count +" times.");
        }
        if(flag == 3){
            System.out.println(text);

            System.out.println(text.replaceAll(getStringSearch(),getStringReplace()));

        }
    }
}
