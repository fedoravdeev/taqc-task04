package org.fedoravdeev.fileparser;

public class Main {
    public static void main(String[] args) {
        Storage storage = new Storage();
        storage.setParam(args);
        storage.run();
    }
}
